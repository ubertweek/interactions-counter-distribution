# UNMATCHED FACEBOOK OBJECT INTERACTIONS DISTRIBUTION in GiantDb
# This compute the number of interactions on

# 1) USING JOINS

SELECT low_interact_object.counter, COUNT(*)
FROM(
	SELECT e.id, COUNT(*) AS counter
	FROM interactions AS i
	INNER JOIN external_objects AS e
	ON i.external_object_id = e.id
	WHERE e.tweek_object_id IS NULL
	AND e.origin = 'facebook'
	GROUP BY e.id
	HAVING COUNT(*) < 15
) low_interact_object
GROUP BY low_interact_object.counter
ORDER BY low_interact_object.counter DESC


# 2) USING interactions_count

SELECT COUNT(*)
FROM external_objects
WHERE tweek_object_id IS NULL
AND origin = 'facebook'
AND interactions_count = 1



# MATCHED FACEBOOK OBJECT INTERACTIONS DISTRIBUTION in GiantDb

# 1) USING JOINS

SELECT low_interact_object.counter, COUNT(*)
FROM(
	SELECT e.id, COUNT(*) AS counter
	FROM interactions AS i
	INNER JOIN external_objects AS e
	ON i.external_object_id = e.id
	WHERE e.tweek_object_id IS NOT NULL
	AND e.origin = 'facebook'
	GROUP BY e.id
	HAVING COUNT(*) = 1
) low_interact_object
GROUP BY low_interact_object.counter
ORDER BY low_interact_object.counter DESC


# 2) Using interaction_count

SELECT COUNT(*)
FROM external_objects
WHERE tweek_object_id IS NOT NULL
AND origin = 'facebook'
AND interactions_count = 1
